from math import log
from sys import getsizeof
import pickle
import binascii
from operator import itemgetter
from haffman import *
from coding import *


def pack(input,output):
    file = list(open(input,'rb')) # 'r' для тексту, 'rb' для бінарних
    lines = [line for line in file]
    lst = []
    for i in lines:
        for symbol in i:
            lst.append(symbol)
    print('Текст:',lst)
    #print('Довжина тексту:',len(lst))
    #print('Розмір тексту:',getsizeof(lst))

    alphabet = {}
    for i in lst:
        if i not in alphabet:
            alphabet[i]=1
        else:
            alphabet[i]+=1

    #print('Характеристика повідомлення',alphabet)

    probability = [alphabet[j]/len(lst) for j in alphabet]
    enthropy = -sum([p*log(p,2) for p in probability])
    print("Ентропія: ",enthropy)

    alphabet = sorted(alphabet.items(), key=itemgetter(1), reverse = True)
    #print('Відсортовані вірогідності: ',alphabet)

    data = haffman_encoding(alphabet,lst)
    data.append('decoded_'+input)

    FILENAME = output
 
    with open(FILENAME, "wb") as file:
        pickle.dump(data, file)      

def unpack(input, output):
    with open(input, "rb") as file:
        data_from_file = pickle.load(file)
        code = text_to_bits(data_from_file[0])
        code = text_to_bits(data_from_file[0])[0:len(code)-data_from_file[1]]
        tree = data_from_file[2]
        filename = data_from_file[3]
        #print('Результат разархивации: ',code,tree,filename)
        text = get_text(code,tree)
        #print(text)
        with open(filename, "wb") as output:
            output.write(bytearray(text))


f = input('Название файла: ')
a = input('Название архива: ')
#f ='text.txt'
#a = '1.lul'

pack(f,a)
unpack(a,f)