import binascii

def string2bits(s=''):
    return [bin(ord(x))[2:].zfill(8) for x in s]

def bits2string(b=None):
    return ''.join([chr(int(x, 2)) for x in b])

def text_to_bits(text):
    return ''.join(string2bits(text))

def text_from_bits(line):
    nulls = 0
    while len(line) % 8 != 0:
        line += '0'
        nulls += 1
    bits = [line[i:i+8] for i in range(0, len(line), 8)]
    return bits2string(bits),nulls


def bits_encode(line):#???
    line = [line[i:i+8] for i in range(0, len(line), 8)]
    result = ''
    nulls = 0
    for i in line:
        r,n = text_from_bits(i,errors='replace')
        result += r
        nulls += n
    return result, nulls