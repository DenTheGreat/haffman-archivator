from operator import itemgetter
from coding import *
from tree import Node
from goto import with_goto

def haffman_iteration(dict):
    try:
        dict = sorted(dict.items(), key=itemgetter(1), reverse = True)
    except AttributeError:
        dict = sorted(dict, key=itemgetter(1), reverse = True)
    last2 = dict[len(dict)-2:]
    dict.pop(len(dict)-1)
    dict.pop(len(dict)-1)

    if isinstance(last2[0][0],Node) and not isinstance(last2[1][0],Node):
        new_node = last2[0][0] + Node(last2[1][0])
    elif isinstance(last2[0][0],Node) and isinstance(last2[1][0],Node):
        new_node = last2[0][0] + last2[1][0]
    elif isinstance(last2[1][0],Node) and not isinstance(last2[0][0],Node):
        new_node = Node(last2[0][0]) + last2[1][0]
    else:
        new_node = Node(last2[0][0]) + Node(last2[1][0])

    dict.append((new_node,last2[0][1]+last2[1][1]))
    return dict

def get_codes_execution(root,code_list,stack,direction = None): # code_list = {}
    if isinstance(root,Node):
        if root.value is None:
            if direction is not None:
                stack.append(direction)
        else:
            stack.append(direction)
            code_list[root.value] = ''.join(stack)
        get_codes_execution(root.left,code_list,stack,'0')
        get_codes_execution(root.right,code_list,stack,'1')
        if len(stack)>0:
            stack.pop(len(stack)-1)

def get_codes(root):
    codes = {}
    stack = []
    get_codes_execution(root,codes,stack,direction = None)
    return codes


def haffman_encoding(alphabet, list):
    while len(alphabet) > 1:
        alphabet = haffman_iteration(alphabet)

    #for i in alphabet: #Вывод дерева в консоль
    #    if isinstance(i[0],Node):
    #        #print(i[0])
    codes = get_codes(alphabet[0][0])
    #print('Коды',codes)

    coded_list = []
    for i in list:
        coded_list.append(codes[i])
    result = ''.join(coded_list)
    #print("Последовательность: ",result)

    encoded,nul = text_from_bits(result)
    #print("Encoded",encoded)
    data = [
        encoded,
        nul,
        alphabet[0][0]
        ]

    return data

@with_goto
def get_text_execution(code,root,head,text):
    label .begin
    if isinstance(root,Node):
        if root.value is not None:
            #print(root.value,chr(root.value),ord(chr(root.value)))
            #text += chr(root.value) #############
            text.append(root.value)
            root = head
        if len(code)>0:
            direction = code[0]
            code = code[1:]            
            if direction is '0':
                root = root.left
                goto .begin
            else:
               root = root.right
               goto .begin  
    return text

def get_text(code,root):
    text = []#u''
    txt = get_text_execution(code,root,root,text)
    #print(txt)
    return txt



