import binascii

def text_to_bits(text, encoding='utf-8', errors='ignore'):
    bits = bin(int(binascii.hexlify(text.encode(encoding, errors)), 16))[2:]
    return bits.zfill(8 * ((len(bits) + 7) // 8))

def text_from_bits(bits, encoding='utf-8', errors='ignore'):
    nulls = 0
    while len(bits) % 8 != 0:
        bits += '0'
        nulls += 1
    n = int(bits, 2)
    return int2bytes(n).decode(encoding, errors), nulls

def int2bytes(i):
    hex_string = '%x' % i
    n = len(hex_string)
    return binascii.unhexlify(hex_string.zfill(n + (n & 1)))

def bits_encode(line):
    line = [line[i:i+8] for i in range(0, len(line), 8)]
    result = ''
    nulls = 0
    for i in line:
        r,n = text_from_bits(i,errors='replace')
        result += r
        nulls += n
    return result, nulls


def get_text_execution(code,root,head,text):
    if isinstance(root,Node):
        if root.value is not None:
            text += str(root.value)
            root = head
        if len(code)>0:
            direction = code[0]
            code = code[1:]            
            if direction is '0':
                return get_text_execution(code,root.left,head,text)
            else:
               return get_text_execution(code,root.right,head,text)    
    return text